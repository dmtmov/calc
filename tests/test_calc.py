import pytest
from src.calc import calc, Operator, Operand


def test_parser():
    with pytest.raises(ValueError):
        assert calc.parse_expression("2+2")

    output = calc.parse_expression("3 + 4 * 2")
    out_operator_1 = output.pop()
    out_operator_2 = output.pop()

    assert isinstance(out_operator_1, Operator)\
           and out_operator_1.symbol == "+"\
           and out_operator_1.priority == 1
    assert isinstance(out_operator_2, Operator) \
           and out_operator_2.symbol == "*" \
           and out_operator_2.priority == 2
    assert all([isinstance(_, Operand) for _ in output])


def test_calculator():
    assert calc.calculate("2 + 2") == (4.0, [])

    # test failure on input
    assert calc.calculate("2+2") == (
        None, ['Invalid token. Make sure you have space between operators.']
    )

    # test priority
    assert calc.calculate("2 + 8 / 2") == (6.0, [])
    assert calc.calculate("2 + 8 / 2 * 2 - 6") == (4.0, [])

    # negative result
    assert calc.calculate("2 + 2 - 3 * 2") == (-2.0, [])

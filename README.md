# Basic Web Calculator

Simple form with the basic operations(might be extended) over two complex expressions.

## Install
Copy repo folder and go into its folder.  
Install dependencies and run web-server:
```shell
poetry install
poetry run uvicorn src.main:app
```
Check links:  
    - API docs: http://127.0.0.1:8000/docs  
    - API service: http://127.0.0.1:8000/api/v1/

## Run tests 
```shell
   poetry run pytest tests 
```

## Roadmap
- [x] API Docs; 
- [x] Complex expressions as one operand;
- [x] Priority of operators in expressions;
- [ ] Add brackets support;
- [ ] Add negative operators support;
- [ ] Support different Clients;

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.routes import web, api


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(web)
app.include_router(api, prefix='/api')

from fastapi import APIRouter
from fastapi.responses import JSONResponse
from src.scheme import RequestModel, ResponseModel
from src.calc import calc


web = APIRouter()
api = APIRouter(tags=['api'], prefix='/v1')


@api.get('/operators')
def list_operators():
    return JSONResponse(list(calc.operators.keys()))


@api.post('/calc')
def calculate(expression: RequestModel):
    result, errors = calc.calculate(expression.expression)
    return ResponseModel(result=result, errors=errors)

from typing import Callable
from operator import add, sub, truediv, mul


class Operand:
    """
    The operator might be int or float type,
    but during calculation we convert the value to float.
    """

    def __init__(self, value: int | float):
        self.value = value

    def evaluate(self):
        return float(self.value)


class Operator:
    def __init__(self, symbol: str, priority: int, function: Callable):
        self.symbol = symbol
        self.priority = priority
        self.function = function

    def evaluate(self, left_operand, right_operand):
        return self.function(left_operand, right_operand)


class Calculator:
    """
    Can be extended with new operators from the `operator` module.
    """
    def __init__(self):
        self.operators = {
            "+": Operator("+", 1, add),
            "-": Operator("-", 1, sub),
            "*": Operator("*", 2, mul),
            "/": Operator("/", 2, truediv),
        }

    def parse_expression(self, expression):
        tokens = expression.split()
        output_queue = []
        operator_stack = []
        for token in tokens:
            if token.isnumeric():
                output_queue.append(Operand(token))
            elif token in self.operators:
                operator = self.operators[token]
                while operator_stack and operator_stack[-1].priority >= operator.priority:
                    output_queue.append(operator_stack.pop())
                operator_stack.append(operator)
            else:
                raise ValueError(
                    "Invalid token. Make sure you have space between operators."
                )
        while operator_stack:
            output_queue.append(operator_stack.pop())
        return output_queue

    def evaluate_expression(self, output_queue):
        operand_stack = []
        for token in output_queue:
            if isinstance(token, Operand):
                operand_stack.append(token)
            elif isinstance(token, Operator):
                if len(operand_stack) < 2:
                    raise ValueError("Invalid expression")
                right_operand = operand_stack.pop()
                left_operand = operand_stack.pop()
                result = token.evaluate(
                    left_operand.evaluate(),
                    right_operand.evaluate()
                )
                operand_stack.append(Operand(result))
            else:
                raise ValueError("Invalid token")
        if len(operand_stack) != 1:
            raise ValueError("Invalid expression")
        return operand_stack[0].evaluate()

    def calculate(self, expression):
        result = None
        errors = []
        try:
            output_queue = self.parse_expression(expression)
            result = self.evaluate_expression(output_queue)
        except Exception as e:
            errors.append(str(e))
        return result, errors


calc = Calculator()

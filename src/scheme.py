from pydantic import BaseModel


class RequestModel(BaseModel):
    expression: str


class ResponseModel(BaseModel):
    result: float | None = None
    errors: list[str | None]
